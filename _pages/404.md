---
layout: page
title: Page In Absentia
---

The page you are looking for does not exist, but if you believe hard enough then maybe, just maybe, you can bring it in existence.

Keep refreshing the page to test your persistence.
