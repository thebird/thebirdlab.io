---
layout: post
title: "genetic pranks"
categories: life
---

It won't be long before genetic engineering becomes as easy to do at home as it now is to edit movies. Something which once required specialized equipment and massive investment will require neither. We're almost there, with hobbyist geneticists running sequencers in their garage and DIY Crispr kits being sold online. It's likely that someday, provided climate change or aliens don't decimate us first, we'll have food which is genetically modified and grown to match our particular biochemical needs. But as with all advancements there comes responsibility and the potential for disaster. I don't want to talk about either of those, but instead about something even more inevitable.

Imagine for a moment that you and your significant other are deep in the throes of passion and you go down on him and he does down on you and it's magical and wonderful and behind closed doors. Also imagine that some 16 year old kid has developed two new strains of cold virus, which spread easily and, for the most part, undetected. These strains cause no traditional symptoms, but instead work in concert. One strain secretes itself through vaginal fluid and pre-cum, while the other works primarily through saliva. When these strains meet it's as if fireworks exploded, causing a profusion of photosensitive waste to be produced. When you are your significant other step into the sun, your mouths turn an array of colors.

It's fun and it's harmless and it's the tip of the fucking iceberg that goes down a solid kilometer.
