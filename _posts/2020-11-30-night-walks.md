---
layout: post
title: "night people"
categories: life
---

I take long walks, mostly at night. I would say it's because I like the isolation, but this city doesn't allow that as much as one would like. Narrow streets, many without sidewalks, and any corner occupied by night people. The old man with a pile of cigarette butts at his feet. A univeristy kid sitting backwards on his parked scooter and playing on his phone. The occasional musician practicing near the river, far from the nearest aparment and complaining residents.

For me it's podcasts and meditation. I have a few spots picked out. Benches along less frequented paths and in parks that grow quiet at night. There's something about night, especially in the fall and winter, where the air tries to crawl between the cracks in your jacket and you need to pull your hood up to keep it at bay for just a little longer.

I appreciate the false sense of stillness against the hum of distant traffic and lights that twinkle like the stars they've replaced. In a way it feels safe, and I like to imagine these other night people see it the same. As the sun goes down we all venture out to enjoy being alone together, away from four walls and into the confines of the city that breathes around us.
