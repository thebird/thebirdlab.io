---
layout: post
title: "no death"
categories: life
---

In 2054 Betty Urbo cured the first person of death. It would be only 17 days later that she should pass away.

A friend once asked me if I saw the same colors that he did. He had eat the better portion of an eighth of mushrooms, but the question, at least that the time and for a 17 year old kid, made me stop and think. It made me see the world in a somewhat different light. Less self-centered, but also more skeptically. What Betty Urbo did was take a small section of those ideas away from us. She took away the ability to acknowledge death.
