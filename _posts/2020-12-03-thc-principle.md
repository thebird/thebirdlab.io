---
layout: post
title: "thc principle"
categories: life
---

The **Turtle Head Cometh** is a principle which states that as the time it would take you to reach the bathroom decreases, the length of the turtle head increases exponentially. More importantly, if no medical issues complicate the matter, the turtle head only approaches its maximum length, never actually reaching it. To put it in layman's terms, the shit don't drop until you're on the pot.

![Picture description](/assets/images/thc.png){: .center-image }

This would beg the question of whether one actually poops or not, given that the turtle head only approaches its maximum length. In truth, you and your poop are always connected, it's just that due to the exponential acceleration at which it approaches its maximum length it has actually moved forward in time and past the end of your life.

This principle applies to many situations, from the eagerness for Christmas to the growing frustration with work as a holiday approaches. As a teacher, this feeling is all too common. Despite relatively short periods of time between holidays, frustration and burn out rapidly increase as the next break nears. This is most acute at Christmas, Spring Break and Summer, though occurs on shorter 5-day and 4-day weekends as well. There is currently no practiced methods by which to prevent THC syndrome.
