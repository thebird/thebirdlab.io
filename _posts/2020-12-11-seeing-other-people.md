---
layout: post
title: "the office odds"
categories: ideas
---

A site where small groups of people can get together and make bets on things that could happen in their daily life. For example, betting on how many times Emily will say "Isn't that just great?" when she leads the next meeting. It should be a small odds side where all bets are resolved by people within the pool. Individuals can post something that others can bet on. Those that help verify the bet will earn a small amount of the other overall betting pool.
