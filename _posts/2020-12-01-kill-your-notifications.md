---
layout: post
title: "notifications"
categories: life
---

If you programmed each of your app notifications to be a different musical note, how long do you think until it would play Funky Town?

I turned off all the notifications on my phone about 5 years ago, but that was mainly because I don't have many friends. I don't miss them. The notifications, not the friends. Those I had are thousands of kilometers away, and those I still have are often too busy. Life inside the warming glow of Netflix and an endless pile of books is enough to keep me occupied. Their life inside their own glowing boxes of splintered wood and concrete blocks is enough for them.
